/* This ocamlyacc file was machine-generated by the BNF converter */
%{
open AbsJava
open Lexing
%}

%token TOK_abstract TOK_boolean TOK_break TOK_byte TOK_case TOK_catch TOK_char TOK_class TOK_continue TOK_default TOK_do TOK_double TOK_else TOK_extends TOK_false TOK_final TOK_finally TOK_float TOK_for TOK_if TOK_implements TOK_import TOK_instanceof TOK_int TOK_interface TOK_long TOK_native TOK_new TOK_null TOK_package TOK_private TOK_protected TOK_public TOK_return TOK_short TOK_static TOK_super TOK_switch TOK_synchronized TOK_this TOK_throw TOK_throws TOK_transient TOK_true TOK_try TOK_volatile TOK_while

%token SYMB1 /* .* */
%token SYMB2 /* { */
%token SYMB3 /* } */
%token SYMB4 /* ; */
%token SYMB5 /* ( */
%token SYMB6 /* ) */
%token SYMB7 /* : */
%token SYMB8 /* = */
%token SYMB9 /* , */
%token SYMB10 /* else if */
%token SYMB11 /* [] */
%token SYMB12 /* . */
%token SYMB13 /* ? */
%token SYMB14 /* || */
%token SYMB15 /* && */
%token SYMB16 /* | */
%token SYMB17 /* ^ */
%token SYMB18 /* & */
%token SYMB19 /* == */
%token SYMB20 /* != */
%token SYMB21 /* < */
%token SYMB22 /* > */
%token SYMB23 /* <= */
%token SYMB24 /* >= */
%token SYMB25 /* << */
%token SYMB26 /* >> */
%token SYMB27 /* >>> */
%token SYMB28 /* + */
%token SYMB29 /* - */
%token SYMB30 /* * */
%token SYMB31 /* / */
%token SYMB32 /* % */
%token SYMB33 /* ++ */
%token SYMB34 /* -- */
%token SYMB35 /* [ */
%token SYMB36 /* ] */
%token SYMB37 /* .this */
%token SYMB38 /* .class */
%token SYMB39 /* ~ */
%token SYMB40 /* ! */
%token SYMB41 /* *= */
%token SYMB42 /* /= */
%token SYMB43 /* %= */
%token SYMB44 /* += */
%token SYMB45 /* -= */
%token SYMB46 /* <<= */
%token SYMB47 /* >>= */
%token SYMB48 /* >>>= */
%token SYMB49 /* &= */
%token SYMB50 /* ^= */
%token SYMB51 /* |= */

%token TOK_EOF
%token <string> TOK_Ident
%token <string> TOK_String
%token <int> TOK_Integer
%token <float> TOK_Double
%token <char> TOK_Char
%token <string> TOK_Unsigned
%token <string> TOK_Long
%token <string> TOK_UnsignedLong
%token <string> TOK_Hexadecimal
%token <string> TOK_HexUnsigned
%token <string> TOK_HexLong
%token <string> TOK_HexUnsLong
%token <string> TOK_Octal
%token <string> TOK_OctalUnsigned
%token <string> TOK_OctalLong
%token <string> TOK_OctalUnsLong
%token <string> TOK_JDouble
%token <string> TOK_JFloat
%token <string> TOK_JLongDouble
%token <string> TOK_UnicodeChar
%token <string> TOK_JChar

%start pProgramFile
%type <AbsJava.programFile> pProgramFile


%%
pProgramFile : programFile TOK_EOF { $1 }
  | error { raise (BNFC_Util.Parse_error (Parsing.symbol_start_pos (), Parsing.symbol_end_pos ())) };


programFile : TOK_package ident_list semi_list import_list typeDecl_list { Prpkg ($2, (List.rev $3), (List.rev $4), (List.rev $5)) } 
  | import_list typeDecl_list { ProgF ((List.rev $1), (List.rev $2)) }
;

import : TOK_import ident_list semi_list { ImportN ($2, (List.rev $3)) } 
  | TOK_import ident_list SYMB1 semi_list { ImportA ($2, (List.rev $4)) }
;

import_list : /* empty */ { []  } 
  | import_list import { (fun (x,xs) -> x::xs) ($2, $1) }
;

typeDecl : classHeader SYMB2 fieldDeclaration_list SYMB3 { TypeDecl ($1, (List.rev $3)) } 
;

typeDecl_list : /* empty */ { []  } 
  | typeDecl_list typeDecl { (fun (x,xs) -> x::xs) ($2, $1) }
;

classHeader : modifier_list TOK_class ident { ClassDec ((List.rev $1), $3) } 
  | modifier_list TOK_class ident TOK_extends typeName_list { ClassDecE ((List.rev $1), $3, $5) }
  | modifier_list TOK_class ident TOK_implements typeName_list { ClassDecI ((List.rev $1), $3, $5) }
  | modifier_list TOK_class ident TOK_extends typeName_list TOK_implements typeName_list { ClassDecEI ((List.rev $1), $3, $5, $7) }
  | modifier_list TOK_interface ident { InterDec ((List.rev $1), $3) }
  | modifier_list TOK_interface ident TOK_extends typeName_list { InterDecE ((List.rev $1), $3, $5) }
  | modifier_list TOK_interface ident TOK_implements typeName_list { InterDecI ((List.rev $1), $3, $5) }
  | modifier_list TOK_interface ident TOK_extends typeName_list TOK_implements typeName_list { InterDecEI ((List.rev $1), $3, $5, $7) }
;

fieldDeclaration : modifier_list typeSpec varDecl_list SYMB4 { Dvar ((List.rev $1), $2, $3) } 
  | modifier_list typeSpec methodDecl methodBody { Dmth ((List.rev $1), $2, $3, $4) }
  | modifier_list typeSpec methodDecl TOK_throws typeName_list methodBody { Dmthth ((List.rev $1), $2, $3, $5, $6) }
  | modifier_list ident SYMB5 parameter_list SYMB6 body { Dconst ((List.rev $1), $2, $4, $6) }
  | modifier_list ident SYMB5 parameter_list SYMB6 TOK_throws typeName_list body { Dconstt ((List.rev $1), $2, $4, $7, $8) }
  | body { Dblk $1 }
  | typeDecl { Dinnerclass $1 }
;

fieldDeclaration_list : /* empty */ { []  } 
  | fieldDeclaration_list fieldDeclaration { (fun (x,xs) -> x::xs) ($2, $1) }
;

methodBody : SYMB4 { IBody  } 
  | body { MBody $1 }
;

lVarStatement : typeSpec varDecl_list SYMB4 { LVar ($1, $2) } 
  | TOK_final typeSpec varDecl_list SYMB4 { LVarf ($2, $3) }
  | stm { Statem $1 }
;

lVarStatement_list : /* empty */ { []  } 
  | lVarStatement_list lVarStatement { (fun (x,xs) -> x::xs) ($2, $1) }
;

body : SYMB2 lVarStatement_list SYMB3 { Body (List.rev $2) } 
;

stm : SYMB4 { Sem  } 
  | ident SYMB7 { Lbl $1 }
  | TOK_case exp SYMB7 { Case $2 }
  | TOK_default SYMB7 { Dflt  }
  | exp SYMB4 { Exps $1 }
  | SYMB2 lVarStatement_list SYMB3 { LV (List.rev $2) }
  | jumpStm { Jmp $1 }
  | guardStm { Grd $1 }
  | iterStm { Iter $1 }
  | selectionStm { Sel $1 }
;

declaratorName : ident { DeclName $1 } 
  | ident bracketsOpt_list { DeclArray ($1, $2) }
;

varDecl : declaratorName SYMB8 variableInits { VDeclAssign ($1, $3) } 
  | ident { VDecl $1 }
;

varDecl_list : varDecl { (fun x -> [x]) $1 } 
  | varDecl SYMB9 varDecl_list { (fun (x,xs) -> x::xs) ($1, $3) }
;

variableInits : exp { IExp $1 } 
  | SYMB2 SYMB3 { IEmpt  }
  | SYMB2 arrayInits SYMB3 { IArri $2 }
;

arrayInits : variableInits { Vainit $1 } 
  | arrayInits SYMB9 variableInits { Vai ($1, $3) }
  | arrayInits SYMB9 { Vais $1 }
;

methodDecl : declaratorName SYMB5 parameter_list SYMB6 { Mth ($1, $3) } 
  | methodDecl bracketsOpt { MthdArr ($1, $2) }
;

parameter : typeSpec declaratorName { Param ($1, $2) } 
  | TOK_final typeSpec declaratorName { Pfinal ($2, $3) }
;

parameter_list : /* empty */ { []  } 
  | parameter { (fun x -> [x]) $1 }
  | parameter SYMB9 parameter_list { (fun (x,xs) -> x::xs) ($1, $3) }
;

selectionStm : TOK_if SYMB5 exp SYMB6 stm elseif_list { Ifone ($3, $5, (List.rev $6)) } 
  | TOK_if SYMB5 exp SYMB6 stm elseif_list TOK_else stm { If ($3, $5, (List.rev $6), $8) }
  | TOK_switch SYMB5 exp SYMB6 body { Switch ($3, $5) }
;

elseif : SYMB10 SYMB5 exp SYMB6 stm { Elseif ($3, $5) } 
;

elseif_list : /* empty */ { []  } 
  | elseif_list elseif { (fun (x,xs) -> x::xs) ($2, $1) }
;

jumpStm : TOK_break SYMB4 { Break  } 
  | TOK_break ident SYMB4 { Brlabel $2 }
  | TOK_continue SYMB4 { Continue  }
  | TOK_continue ident SYMB4 { Continuelabel $2 }
  | TOK_return SYMB4 { Return  }
  | TOK_return exp SYMB4 { ReturnExp $2 }
  | TOK_throw exp SYMB4 { Throw $2 }
;

guardStm : TOK_synchronized SYMB5 exp SYMB6 body { Synchronized ($3, $5) } 
  | TOK_try body catch_list { Try ($2, (List.rev $3)) }
  | TOK_try body catch_list TOK_finally body { Tryfinally ($2, (List.rev $3), $5) }
;

catch : TOK_catch SYMB5 typeSpec ident SYMB6 body { Catch1 ($3, $4, $6) } 
  | TOK_catch SYMB5 typeSpec SYMB6 body { Catch2 ($3, $5) }
;

catch_list : /* empty */ { []  } 
  | catch_list catch { (fun (x,xs) -> x::xs) ($2, $1) }
;

iterStm : TOK_while SYMB5 exp SYMB6 stm { While ($3, $5) } 
  | TOK_do stm TOK_while SYMB5 exp SYMB6 { Do ($2, $5) }
  | TOK_for SYMB5 forInit exp_list SYMB4 exp_list SYMB6 stm { For ($3, $4, $6, $8) }
;

forInit : exp_list SYMB4 { Exprs1 $1 } 
  | typeSpec varDecl_list SYMB4 { DVar ($1, $2) }
  | TOK_final typeSpec varDecl_list SYMB4 { DVarf ($2, $3) }
;

modifier : TOK_abstract { Mabstract  } 
  | TOK_final { Mfinal  }
  | TOK_public { Mpublic  }
  | TOK_protected { Mprotected  }
  | TOK_private { Mprivate  }
  | TOK_transient { Mtransient  }
  | TOK_volatile { Mvolatile  }
  | TOK_native { Mnative  }
  | TOK_synchronized { Msync  }
  | TOK_static { Mstatic  }
;

modifier_list : /* empty */ { []  } 
  | modifier_list modifier { (fun (x,xs) -> x::xs) ($2, $1) }
;

basicType : TOK_char { Tchar  } 
  | TOK_short { Tshort  }
  | TOK_int { Tint  }
  | TOK_long { Tlong  }
  | TOK_float { Tfloat  }
  | TOK_double { Tdouble  }
  | TOK_byte { Tbyte  }
  | TOK_boolean { Tboolean  }
;

typeSpec : typeName bracketsOpt_list { ArrayType ($1, $2) } 
  | typeName { TypeName $1 }
;

typeName : basicType { BuiltIn $1 } 
  | ident_list { ClassType $1 }
;

typeName_list : typeName { (fun x -> [x]) $1 } 
  | typeName SYMB9 typeName_list { (fun (x,xs) -> x::xs) ($1, $3) }
;

bracketsOpt : SYMB11 { BracketsOpt  } 
;

bracketsOpt_list : bracketsOpt { (fun x -> [x]) $1 } 
  | bracketsOpt bracketsOpt_list { (fun (x,xs) -> x::xs) ($1, $2) }
;

ident_list : ident { (fun x -> [x]) $1 } 
  | ident SYMB12 ident_list { (fun (x,xs) -> x::xs) ($1, $3) }
;

exp : exp14 assignment_op exp { Eassign ($1, $2, $3) } 
  | exp14 TOK_instanceof typeName { Etype ($1, $3) }
  | exp1 {  $1 }
;

exp2 : exp3 SYMB13 exp SYMB7 exp2 { Econdition ($1, $3, $5) } 
  | exp3 {  $1 }
;

exp3 : exp3 SYMB14 exp4 { Elor ($1, $3) } 
  | exp4 {  $1 }
;

exp4 : exp4 SYMB15 exp5 { Eland ($1, $3) } 
  | exp5 {  $1 }
;

exp5 : exp5 SYMB16 exp6 { Ebitor ($1, $3) } 
  | exp6 {  $1 }
;

exp6 : exp6 SYMB17 exp7 { Ebitexor ($1, $3) } 
  | exp7 {  $1 }
;

exp7 : exp7 SYMB18 exp8 { Ebitand ($1, $3) } 
  | exp8 {  $1 }
;

exp8 : exp8 SYMB19 exp9 { Eeq ($1, $3) } 
  | exp8 SYMB20 exp9 { Eneq ($1, $3) }
  | exp9 {  $1 }
;

exp9 : exp9 SYMB21 exp10 { Elthen ($1, $3) } 
  | exp9 SYMB22 exp10 { Egrthen ($1, $3) }
  | exp9 SYMB23 exp10 { Ele ($1, $3) }
  | exp9 SYMB24 exp10 { Ege ($1, $3) }
  | exp10 {  $1 }
;

exp10 : exp10 SYMB25 exp11 { Eleft ($1, $3) } 
  | exp10 SYMB26 exp11 { Eright ($1, $3) }
  | exp10 SYMB27 exp11 { Etrip ($1, $3) }
  | exp11 {  $1 }
;

exp11 : exp11 SYMB28 exp12 { Eplus ($1, $3) } 
  | exp11 SYMB29 exp12 { Eminus ($1, $3) }
  | exp12 {  $1 }
;

exp12 : exp12 SYMB30 exp13 { Etimes ($1, $3) } 
  | exp12 SYMB31 exp13 { Ediv ($1, $3) }
  | exp12 SYMB32 exp13 { Emod ($1, $3) }
  | exp13 {  $1 }
;

exp13 : SYMB5 basicType SYMB6 exp13 { Ebcoercion ($2, $4) } 
  | SYMB5 exp SYMB6 exp15 { Eexpcoercion ($2, $4) }
  | SYMB5 ident_list bracketsOpt_list SYMB6 exp13 { Earrcoercion ($2, $3, $5) }
  | exp14 {  $1 }
;

exp14 : unary_operator exp13 { Epreop ($1, $2) } 
  | SYMB33 exp14 { Epreinc $2 }
  | SYMB34 exp14 { Epredec $2 }
  | exp15 {  $1 }
;

exp15 : exp15 SYMB33 { Epostinc $1 } 
  | exp15 SYMB34 { Epostdec $1 }
  | exp16 {  $1 }
;

exp16 : specName { Especname $1 } 
  | arrAcc { Earr $1 }
  | mthCall { Emth $1 }
  | fieldAcc { Efld $1 }
  | constant { Econst $1 }
  | string { Estring $1 }
  | exp17 {  $1 }
;

exp17 : newAlloc { Enewalloc $1 } 
  | exp18 {  $1 }
;

exp18 : ident_list { Evar $1 } 
  | SYMB5 exp SYMB6 {  $2 }
;

specName : TOK_super { SSsuper  } 
  | TOK_this { SSthis  }
  | TOK_null { SSnull  }
;

newAlloc : TOK_new typeName args { Anewclass ($2, $3) } 
  | TOK_new typeName args SYMB2 fieldDeclaration_list SYMB3 { Anewinnerclass ($2, $3, (List.rev $5)) }
  | TOK_new typeName dimExpr_list { Anewarray ($2, $3) }
  | TOK_new typeName dimExpr_list SYMB2 SYMB3 { Anewarriempty ($2, $3) }
  | TOK_new typeName dimExpr_list SYMB2 arrayInits SYMB3 { Anewarrinits ($2, $3, $5) }
;

arrAcc : ident_list SYMB35 exp SYMB36 { Aarr ($1, $3) } 
  | specExp SYMB35 exp SYMB36 { Aarr1 ($1, $3) }
;

specExp : SYMB5 exp SYMB6 { Cep $2 } 
  | specExpNP { Cnp $1 }
  | specName { Cthis $1 }
;

specExpNP : constant { CNLit $1 } 
  | arrAcc { CNParr $1 }
  | mthCall { CNPmth $1 }
  | fieldAcc { CNPfld $1 }
;

mthCall : ident_list args { Mmth ($1, $2) } 
  | specExpNP args { Mmth1 ($1, $2) }
  | specName args { Mmthspec ($1, $2) }
;

fieldAcc : specExp SYMB12 ident { Ffvar ($1, $3) } 
  | newAlloc SYMB12 ident { Ffvar1 ($1, $3) }
  | ident_list SYMB37 { Ffthis $1 }
  | ident_list SYMB38 { Fclass $1 }
  | basicType SYMB38 { Ffclass2 $1 }
;

args : SYMB5 exp_list SYMB6 { Args $2 } 
;

dimExpr : SYMB35 exp SYMB36 { DimExpr $2 } 
;

dimExpr_list : dimExpr { (fun x -> [x]) $1 } 
  | dimExpr dimExpr_list { (fun (x,xs) -> x::xs) ($1, $2) }
;

exp_list : /* empty */ { []  } 
  | exp { (fun x -> [x]) $1 }
  | exp SYMB9 exp_list { (fun (x,xs) -> x::xs) ($1, $3) }
;

exp1 : exp2 {  $1 } 
;

constant : float { Efloat $1 } 
  | jChar { Echar $1 }
  | unicodeChar { Eunicode $1 }
  | unsigned { Eunsigned $1 }
  | long { Elong $1 }
  | unsignedLong { Eunsignlong $1 }
  | hexadecimal { Ehexadec $1 }
  | hexUnsigned { Ehexaunsign $1 }
  | hexLong { Ehexalong $1 }
  | hexUnsLong { Ehexaunslong $1 }
  | octal { Eoctal $1 }
  | octalUnsigned { Eoctalunsign $1 }
  | octalLong { Eoctallong $1 }
  | octalUnsLong { Eoctalunslong $1 }
  | jDouble { Ecdouble $1 }
  | jFloat { Ecfloat $1 }
  | jLongDouble { Eclongdouble $1 }
  | int { Eint $1 }
  | TOK_true { Etrue  }
  | TOK_false { Efalse  }
;

unary_operator : SYMB28 { Plus  } 
  | SYMB29 { Negative  }
  | SYMB39 { Complement  }
  | SYMB40 { Logicalneg  }
;

assignment_op : SYMB8 { Assign  } 
  | SYMB41 { AssignMul  }
  | SYMB42 { AssignDiv  }
  | SYMB43 { AssignMod  }
  | SYMB44 { AssignAdd  }
  | SYMB45 { AssignSub  }
  | SYMB46 { AssignLeft  }
  | SYMB47 { AssignRight  }
  | SYMB48 { AssignTrip  }
  | SYMB49 { AssignAnd  }
  | SYMB50 { AssignXor  }
  | SYMB51 { AssignOr  }
;

semi : SYMB4 { Sem1  } 
;

semi_list : /* empty */ { []  } 
  | semi_list semi { (fun (x,xs) -> x::xs) ($2, $1) }
;


ident : TOK_Ident  { Ident $1 };
string : TOK_String { $1 };
float : TOK_Double  { $1 };
int :  TOK_Integer  { $1 };
unsigned : TOK_Unsigned { Unsigned ($1)};
long : TOK_Long { Long ($1)};
unsignedLong : TOK_UnsignedLong { UnsignedLong ($1)};
hexadecimal : TOK_Hexadecimal { Hexadecimal ($1)};
hexUnsigned : TOK_HexUnsigned { HexUnsigned ($1)};
hexLong : TOK_HexLong { HexLong ($1)};
hexUnsLong : TOK_HexUnsLong { HexUnsLong ($1)};
octal : TOK_Octal { Octal ($1)};
octalUnsigned : TOK_OctalUnsigned { OctalUnsigned ($1)};
octalLong : TOK_OctalLong { OctalLong ($1)};
octalUnsLong : TOK_OctalUnsLong { OctalUnsLong ($1)};
jDouble : TOK_JDouble { JDouble ($1)};
jFloat : TOK_JFloat { JFloat ($1)};
jLongDouble : TOK_JLongDouble { JLongDouble ($1)};
unicodeChar : TOK_UnicodeChar { UnicodeChar ($1)};
jChar : TOK_JChar { JChar ($1)};


