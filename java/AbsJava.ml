(* OCaml module generated by the BNF converter *)


type ident = Ident of string
and unsigned = Unsigned of string
and long = Long of string
and unsignedLong = UnsignedLong of string
and hexadecimal = Hexadecimal of string
and hexUnsigned = HexUnsigned of string
and hexLong = HexLong of string
and hexUnsLong = HexUnsLong of string
and octal = Octal of string
and octalUnsigned = OctalUnsigned of string
and octalLong = OctalLong of string
and octalUnsLong = OctalUnsLong of string
and jDouble = JDouble of string
and jFloat = JFloat of string
and jLongDouble = JLongDouble of string
and unicodeChar = UnicodeChar of string
and jChar = JChar of string
and programFile =
   Prpkg of ident list * semi list * import list * typeDecl list
 | ProgF of import list * typeDecl list

and import =
   ImportN of ident list * semi list
 | ImportA of ident list * semi list

and typeDecl =
   TypeDecl of classHeader * fieldDeclaration list

and classHeader =
   ClassDec of modifier list * ident
 | ClassDecE of modifier list * ident * typeName list
 | ClassDecI of modifier list * ident * typeName list
 | ClassDecEI of modifier list * ident * typeName list * typeName list
 | InterDec of modifier list * ident
 | InterDecE of modifier list * ident * typeName list
 | InterDecI of modifier list * ident * typeName list
 | InterDecEI of modifier list * ident * typeName list * typeName list

and fieldDeclaration =
   Dvar of modifier list * typeSpec * varDecl list
 | Dmth of modifier list * typeSpec * methodDecl * methodBody
 | Dmthth of modifier list * typeSpec * methodDecl * typeName list * methodBody
 | Dconst of modifier list * ident * parameter list * body
 | Dconstt of modifier list * ident * parameter list * typeName list * body
 | Dblk of body
 | Dinnerclass of typeDecl

and methodBody =
   IBody
 | MBody of body

and lVarStatement =
   LVar of typeSpec * varDecl list
 | LVarf of typeSpec * varDecl list
 | Statem of stm

and body =
   Body of lVarStatement list

and stm =
   Sem
 | Lbl of ident
 | Case of exp
 | Dflt
 | Exps of exp
 | LV of lVarStatement list
 | Jmp of jumpStm
 | Grd of guardStm
 | Iter of iterStm
 | Sel of selectionStm

and declaratorName =
   DeclName of ident
 | DeclArray of ident * bracketsOpt list

and varDecl =
   VDeclAssign of declaratorName * variableInits
 | VDecl of ident

and variableInits =
   IExp of exp
 | IEmpt
 | IArri of arrayInits

and arrayInits =
   Vainit of variableInits
 | Vai of arrayInits * variableInits
 | Vais of arrayInits

and methodDecl =
   Mth of declaratorName * parameter list
 | MthdArr of methodDecl * bracketsOpt

and parameter =
   Param of typeSpec * declaratorName
 | Pfinal of typeSpec * declaratorName

and selectionStm =
   Ifone of exp * stm * elseif list
 | If of exp * stm * elseif list * stm
 | Switch of exp * body

and elseif =
   Elseif of exp * stm

and jumpStm =
   Break
 | Brlabel of ident
 | Continue
 | Continuelabel of ident
 | Return
 | ReturnExp of exp
 | Throw of exp

and guardStm =
   Synchronized of exp * body
 | Try of body * catch list
 | Tryfinally of body * catch list * body

and catch =
   Catch1 of typeSpec * ident * body
 | Catch2 of typeSpec * body

and iterStm =
   While of exp * stm
 | Do of stm * exp
 | For of forInit * exp list * exp list * stm

and forInit =
   Exprs1 of exp list
 | DVar of typeSpec * varDecl list
 | DVarf of typeSpec * varDecl list

and modifier =
   Mabstract
 | Mfinal
 | Mpublic
 | Mprotected
 | Mprivate
 | Mtransient
 | Mvolatile
 | Mnative
 | Msync
 | Mstatic

and basicType =
   Tchar
 | Tshort
 | Tint
 | Tlong
 | Tfloat
 | Tdouble
 | Tbyte
 | Tboolean

and typeSpec =
   ArrayType of typeName * bracketsOpt list
 | TypeName of typeName

and typeName =
   BuiltIn of basicType
 | ClassType of ident list

and bracketsOpt =
   BracketsOpt

and exp =
   Eassign of exp * assignment_op * exp
 | Etype of exp * typeName
 | Econdition of exp * exp * exp
 | Elor of exp * exp
 | Eland of exp * exp
 | Ebitor of exp * exp
 | Ebitexor of exp * exp
 | Ebitand of exp * exp
 | Eeq of exp * exp
 | Eneq of exp * exp
 | Elthen of exp * exp
 | Egrthen of exp * exp
 | Ele of exp * exp
 | Ege of exp * exp
 | Eleft of exp * exp
 | Eright of exp * exp
 | Etrip of exp * exp
 | Eplus of exp * exp
 | Eminus of exp * exp
 | Etimes of exp * exp
 | Ediv of exp * exp
 | Emod of exp * exp
 | Ebcoercion of basicType * exp
 | Eexpcoercion of exp * exp
 | Earrcoercion of ident list * bracketsOpt list * exp
 | Epreop of unary_operator * exp
 | Epreinc of exp
 | Epredec of exp
 | Epostinc of exp
 | Epostdec of exp
 | Especname of specName
 | Earr of arrAcc
 | Emth of mthCall
 | Efld of fieldAcc
 | Econst of constant
 | Estring of string
 | Enewalloc of newAlloc
 | Evar of ident list

and specName =
   SSsuper
 | SSthis
 | SSnull

and newAlloc =
   Anewclass of typeName * args
 | Anewinnerclass of typeName * args * fieldDeclaration list
 | Anewarray of typeName * dimExpr list
 | Anewarriempty of typeName * dimExpr list
 | Anewarrinits of typeName * dimExpr list * arrayInits

and arrAcc =
   Aarr of ident list * exp
 | Aarr1 of specExp * exp

and specExp =
   Cep of exp
 | Cnp of specExpNP
 | Cthis of specName

and specExpNP =
   CNLit of constant
 | CNParr of arrAcc
 | CNPmth of mthCall
 | CNPfld of fieldAcc

and mthCall =
   Mmth of ident list * args
 | Mmth1 of specExpNP * args
 | Mmthspec of specName * args

and fieldAcc =
   Ffvar of specExp * ident
 | Ffvar1 of newAlloc * ident
 | Ffthis of ident list
 | Fclass of ident list
 | Ffclass2 of basicType

and args =
   Args of exp list

and dimExpr =
   DimExpr of exp

and constant =
   Efloat of float
 | Echar of jChar
 | Eunicode of unicodeChar
 | Eunsigned of unsigned
 | Elong of long
 | Eunsignlong of unsignedLong
 | Ehexadec of hexadecimal
 | Ehexaunsign of hexUnsigned
 | Ehexalong of hexLong
 | Ehexaunslong of hexUnsLong
 | Eoctal of octal
 | Eoctalunsign of octalUnsigned
 | Eoctallong of octalLong
 | Eoctalunslong of octalUnsLong
 | Ecdouble of jDouble
 | Ecfloat of jFloat
 | Eclongdouble of jLongDouble
 | Eint of int
 | Etrue
 | Efalse
 | Elonger of int
 | Edouble of float

and unary_operator =
   Plus
 | Negative
 | Complement
 | Logicalneg

and assignment_op =
   Assign
 | AssignMul
 | AssignDiv
 | AssignMod
 | AssignAdd
 | AssignSub
 | AssignLeft
 | AssignRight
 | AssignTrip
 | AssignAnd
 | AssignXor
 | AssignOr

and semi =
   Sem1

