type token =
  | TOK_abstract
  | TOK_boolean
  | TOK_break
  | TOK_byte
  | TOK_case
  | TOK_catch
  | TOK_char
  | TOK_class
  | TOK_continue
  | TOK_default
  | TOK_do
  | TOK_double
  | TOK_else
  | TOK_extends
  | TOK_false
  | TOK_final
  | TOK_finally
  | TOK_float
  | TOK_for
  | TOK_if
  | TOK_implements
  | TOK_import
  | TOK_instanceof
  | TOK_int
  | TOK_interface
  | TOK_long
  | TOK_native
  | TOK_new
  | TOK_null
  | TOK_package
  | TOK_private
  | TOK_protected
  | TOK_public
  | TOK_return
  | TOK_short
  | TOK_static
  | TOK_super
  | TOK_switch
  | TOK_synchronized
  | TOK_this
  | TOK_throw
  | TOK_throws
  | TOK_transient
  | TOK_true
  | TOK_try
  | TOK_volatile
  | TOK_while
  | SYMB1
  | SYMB2
  | SYMB3
  | SYMB4
  | SYMB5
  | SYMB6
  | SYMB7
  | SYMB8
  | SYMB9
  | SYMB10
  | SYMB11
  | SYMB12
  | SYMB13
  | SYMB14
  | SYMB15
  | SYMB16
  | SYMB17
  | SYMB18
  | SYMB19
  | SYMB20
  | SYMB21
  | SYMB22
  | SYMB23
  | SYMB24
  | SYMB25
  | SYMB26
  | SYMB27
  | SYMB28
  | SYMB29
  | SYMB30
  | SYMB31
  | SYMB32
  | SYMB33
  | SYMB34
  | SYMB35
  | SYMB36
  | SYMB37
  | SYMB38
  | SYMB39
  | SYMB40
  | SYMB41
  | SYMB42
  | SYMB43
  | SYMB44
  | SYMB45
  | SYMB46
  | SYMB47
  | SYMB48
  | SYMB49
  | SYMB50
  | SYMB51
  | TOK_EOF
  | TOK_Ident of (string)
  | TOK_String of (string)
  | TOK_Integer of (int)
  | TOK_Double of (float)
  | TOK_Char of (char)
  | TOK_Unsigned of (string)
  | TOK_Long of (string)
  | TOK_UnsignedLong of (string)
  | TOK_Hexadecimal of (string)
  | TOK_HexUnsigned of (string)
  | TOK_HexLong of (string)
  | TOK_HexUnsLong of (string)
  | TOK_Octal of (string)
  | TOK_OctalUnsigned of (string)
  | TOK_OctalLong of (string)
  | TOK_OctalUnsLong of (string)
  | TOK_JDouble of (string)
  | TOK_JFloat of (string)
  | TOK_JLongDouble of (string)
  | TOK_UnicodeChar of (string)
  | TOK_JChar of (string)

val pProgramFile :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> AbsJava.programFile
