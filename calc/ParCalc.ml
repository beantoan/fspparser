type token =
  | SYMB1
  | TOK_EOF
  | TOK_Ident of (string)
  | TOK_String of (string)
  | TOK_Integer of (int)
  | TOK_Double of (float)
  | TOK_Char of (char)

open Parsing;;
let _ = parse_error;;
# 3 "ParCalc.mly"
open AbsCalc
open Lexing
# 16 "ParCalc.ml"
let yytransl_const = [|
  257 (* SYMB1 *);
  258 (* TOK_EOF *);
    0|]

let yytransl_block = [|
  259 (* TOK_Ident *);
  260 (* TOK_String *);
  261 (* TOK_Integer *);
  262 (* TOK_Double *);
  263 (* TOK_Char *);
    0|]

let yylhs = "\255\255\
\001\000\001\000\002\000\002\000\003\000\003\000\004\000\005\000\
\000\000\000\000"

let yylen = "\002\000\
\002\000\001\000\002\000\001\000\003\000\001\000\001\000\001\000\
\002\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\000\000\002\000\008\000\009\000\000\000\006\000\
\007\000\004\000\010\000\000\000\000\000\001\000\003\000\005\000"

let yydgoto = "\003\000\
\006\000\011\000\007\000\008\000\009\000"

let yysindex = "\002\000\
\001\255\002\255\000\000\000\000\000\000\000\000\007\255\000\000\
\000\000\000\000\000\000\003\255\005\255\000\000\000\000\000\000"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"

let yygindex = "\000\000\
\000\000\000\000\000\000\254\255\000\000"

let yytablesize = 11
let yytable = "\012\000\
\004\000\010\000\001\000\002\000\015\000\005\000\005\000\013\000\
\014\000\005\000\016\000"

let yycheck = "\002\000\
\000\001\000\001\001\000\002\000\002\001\005\001\005\001\001\001\
\002\001\005\001\013\000"

let yynames_const = "\
  SYMB1\000\
  TOK_EOF\000\
  "

let yynames_block = "\
  TOK_Ident\000\
  TOK_String\000\
  TOK_Integer\000\
  TOK_Double\000\
  TOK_Char\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'exp) in
    Obj.repr(
# 24 "ParCalc.mly"
                   ( _1 )
# 85 "ParCalc.ml"
               : AbsCalc.exp))
; (fun __caml_parser_env ->
    Obj.repr(
# 25 "ParCalc.mly"
          ( raise (BNFC_Util.Parse_error (Parsing.symbol_start_pos (), Parsing.symbol_end_pos ())) )
# 91 "ParCalc.ml"
               : AbsCalc.exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'num) in
    Obj.repr(
# 27 "ParCalc.mly"
                   ( _1 )
# 98 "ParCalc.ml"
               : AbsCalc.num))
; (fun __caml_parser_env ->
    Obj.repr(
# 28 "ParCalc.mly"
          ( raise (BNFC_Util.Parse_error (Parsing.symbol_start_pos (), Parsing.symbol_end_pos ())) )
# 104 "ParCalc.ml"
               : AbsCalc.num))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'exp) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'num) in
    Obj.repr(
# 31 "ParCalc.mly"
                    ( EPlus (_1, _3) )
# 112 "ParCalc.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'num) in
    Obj.repr(
# 32 "ParCalc.mly"
        ( ENum _1 )
# 119 "ParCalc.ml"
               : 'exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'int) in
    Obj.repr(
# 35 "ParCalc.mly"
          ( NOne _1 )
# 126 "ParCalc.ml"
               : 'num))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : int) in
    Obj.repr(
# 39 "ParCalc.mly"
                    ( _1 )
# 133 "ParCalc.ml"
               : 'int))
(* Entry pExp *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
(* Entry pNum *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let pExp (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : AbsCalc.exp)
let pNum (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 2 lexfun lexbuf : AbsCalc.num)
