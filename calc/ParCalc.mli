type token =
  | SYMB1
  | TOK_EOF
  | TOK_Ident of (string)
  | TOK_String of (string)
  | TOK_Integer of (int)
  | TOK_Double of (float)
  | TOK_Char of (char)

val pExp :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> AbsCalc.exp
val pNum :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> AbsCalc.num
