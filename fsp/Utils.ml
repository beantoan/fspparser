(* http://blog.rktmb.org/search?q=ocaml+random+string+and+word+generation *)

let rand_chr () = 
  Random.self_init();
  (Char.chr (97 + (Random.int 26)))
;;   

let rec rand_voy () = 
  let got = (rand_chr ()) in match got with    
  | 'a' 
  | 'e' 
  | 'i' 
  | 'o' 
  | 'u' 
  | 'y' ->  got    
  | _ -> rand_voy ()
;;        


let rec rand_con () =  let got = (rand_chr ())   in    match got with      
  | 'a' 
  | 'e' 
  | 'i' 
  | 'o' 
  | 'u' 
  | 'y' ->  rand_con ()     
  | _ -> got 
;;        

let rec rand_convoy acc syll_number () = match syll_number with    
  | 0 -> acc;   
  | _ -> rand_convoy (acc ^ (Char.escaped (rand_con ())) ^ (Char.escaped(rand_voy()))) (syll_number - 1) ()
;;        

let rand_word () = 
  Random.self_init();
  (rand_convoy "" (3 + (Random.int 3)) ())
;; 

let rand_name () = 
  Random.self_init();
  rand_convoy "" 3 ()
;;